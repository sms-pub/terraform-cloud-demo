terraform {
  required_version = ">= 0.12.29"
    backend "remote" {
    organization = "sms"

    workspaces {
      name = "terraform-cloud-demo"
    }
  }
}

provider "aws" {
  version = "~> 2.64"
  region = var.aws_region
}

//data "aws_vpc" "demo" {
//  filter {
//    name = "name"
//    values = ["aws-controltower-VPC"]
//  }
//}

resource "aws_instance" "ubuntu" {
  ami               = var.ami_id
  instance_type     = var.instance_type
  availability_zone = "${var.aws_region}a"
  subnet_id         = "subnet-081825de2d4e58647"

  tags = {
    Name = var.name
  }
}