The [scripts](https://github.com/hashicorp/terraform-guides/tree/master/operations/variable-scripts) in this directory 
were copied from the [hashicorp/terraform-guides](https://github.com/hashicorp/terraform-guides) repository on GitHub

Here is an example CSV entry:
```
key;value;<variable type: terraform or env>;<HCL true or false>;<sensitive: true or false>;description
aws_region;us-east-1;terraform;false;false;preferred region
```

Here are two examples for running the set_variables.sh script:
```
./set-variables.sh test-ws
./set-variables.sh test-ws other-variables.csv
```